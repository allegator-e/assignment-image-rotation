#ifndef FILE_IO_H
#define FILE_IO_H

#include "status.h"
#include <stdbool.h>
#include <stdio.h>

enum open_status file_open(const char* name, FILE** file, const char* mode);
enum close_status file_close(FILE* file);

#endif //FILE_IO_H
