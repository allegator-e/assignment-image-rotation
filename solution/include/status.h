#ifndef STATUS_H
#define STATUS_H

enum open_status  {
    OPEN_OK = 0,
    OPEN_INVALID_FILE_NAME,
    OPEN_ERROR
};

enum  close_status  {
    CLOSE_OK = 0,
    CLOSE_ERROR
};
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_EBADF
};
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_INVALID_BITS,
    WRITE_INVALID_HEADER,
    WRITE_EBADF
};

#endif //STATUS_H

