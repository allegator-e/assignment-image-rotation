#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};

struct image image_create (uint64_t width, uint64_t height);
void image_free(struct image* image);

struct pixel get_pixel(struct image *img, uint32_t x, uint32_t y);
void set_pixel(struct image *img, struct pixel color, uint32_t x, uint32_t y);


#endif //IMAGE_H

