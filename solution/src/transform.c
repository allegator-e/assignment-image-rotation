#include "../include/transform.h"
#include <stdlib.h>

struct pixel get_pixel(struct image *img, uint32_t x, uint32_t y) {
    return img->data[x + img->width*y];
}


void set_pixel(struct image *img, struct pixel color, uint32_t x, uint32_t y) {
    img->data[y*img->width + x] = color;
}

struct image rotate( struct image source ) {
    struct image new_image = image_create(source.height,source.width);
    for (size_t y = 0; y < source.height; y++) {
        for (size_t x = 0; x < source.width; x++) {
            set_pixel(&new_image, get_pixel(&source, x, y), (source.height-y-1), x );
        }
    }
    return new_image;
}
