#include "../include/file.h"
#include <stdio.h>

enum open_status file_open(const char* name, FILE** file, const char* mode) {
    if (!name) {
        return OPEN_INVALID_FILE_NAME;
    }
    *file = fopen(name, mode);
    if (file == NULL) {
        return OPEN_ERROR;
    }
    return OPEN_OK;
}


enum close_status file_close(FILE* file) {
    if(fclose(file) == EOF) {
        return CLOSE_ERROR;
    }
    return CLOSE_OK;
}
