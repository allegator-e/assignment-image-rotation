#include "../include/bmp.h"
#include "../include/file.h"
#include "../include/transform.h"
#include <stdio.h>

int main(int argc, char** argv) {
    if (argc < 3) {
        fprintf(stderr,"Недостаточно аргументов");
        return 1;
    }

    FILE* in_file;
    FILE* out_file;
    int fail = 1;

    struct image img;
    struct image new_image;

    if (file_open(argv[1],&in_file,"rb")) {
        fprintf(stderr,"Неудалось открыть файл");
        return 1;
    }
    if (from_bmp(in_file, &img)) {
        fprintf(stderr,"Неудалось считать информацию");
        file_close(in_file);
        goto FAIL;
    }
    
    file_close(in_file);

    new_image = rotate(img);

    if (file_open(argv[2],&out_file,"wb")) {
        fprintf(stderr,"Неудалось открыть файл для записи");
        goto FAIL;
    }
    if (to_bmp(out_file,&new_image)) {
        fprintf(stderr,"Неудалось записать информацию");
        goto FAIL;
    }
    file_close(out_file);
    
    fail = 0;
    FAIL:
    fprintf(stderr,"Kjk");
    image_free(&img);
    fprintf(stderr,"Kjk2");
    image_free(&new_image);
    fprintf(stderr,"Kjk3");
    return fail;
}
