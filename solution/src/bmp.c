#include "../include/bmp.h"
#include "../include/status.h"

#define BMP_TYPE 19778
#define BMP_BI_SIZE 40
#define BMP_PLANES 1
#define BMP_BIT_COUNT 24
#define BMP_COMPRESSION 0

static inline size_t calculate_padding (uint32_t width) {
    return 4 - width * sizeof(struct pixel) % 4;
}

static inline size_t image_size(const struct image *image) {
    return (image->width * sizeof(struct pixel) + calculate_padding(image->width)) * image->height;
}

static inline size_t file_size(const struct image *image) {
    return image_size(image) + sizeof(struct bmp_header);
}
struct bmp_header bpm_header_create(const struct image *image) {
    return (struct bmp_header) {
        .bfType = BMP_TYPE,
        .bfileSize = file_size(image),
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = BMP_BI_SIZE,
        .biWidth = image->width,
        .biHeight = image->height,
        .biPlanes = BMP_PLANES,
        .biBitCount = BMP_BIT_COUNT,
        .biCompression = BMP_COMPRESSION,
        .biSizeImage = image_size(image),
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
}

static enum read_status read_pixel_array(FILE* in, struct image* img) {
    const size_t padding = calculate_padding(img->width);
    for (size_t i = 0; i < img->height; ++i) {
        if (fread(&img->data[i * img->width], sizeof(struct pixel), img->width, in) != img->width) {
            image_free(img);
            return READ_INVALID_BITS;
        }
        if(fseek(in, padding, SEEK_CUR)) {
           image_free(img);
           return READ_EBADF;
        }
    }
    return READ_OK;

}

enum read_status from_bmp( FILE* in, struct image* img ) {

    struct bmp_header header = {0};
    
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }
    if (fseek(in, header.bOffBits, SEEK_SET)) {
        return READ_EBADF;
    }
    *img = image_create(header.biWidth, header.biHeight);

    return read_pixel_array(in, img);
}


static enum write_status write_pixel_array(FILE* out, struct image const* img) {
    const size_t padding = calculate_padding(img->width);
    for (size_t i = 0; i < img->height; ++i) {
        if (fwrite(&img->data[i * img->width], sizeof(struct pixel), img->width, out) != img->width) {
            return WRITE_INVALID_BITS;
        }
        if (fseek(out, padding, SEEK_CUR))
            return WRITE_EBADF;
    }
    return WRITE_OK;

}


enum write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header header = bpm_header_create(img);
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out))
        return WRITE_INVALID_HEADER;
    if (fseek(out, header.bOffBits, SEEK_SET)) {
        return WRITE_EBADF;
    }
    return write_pixel_array(out, img);

}


