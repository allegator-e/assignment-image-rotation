#include "../include/image.h"
#include <malloc.h>


struct image image_create (uint64_t width, uint64_t height) {
    return (struct image) {
        .width = width,
        .height = height,
        .data = malloc(sizeof(struct pixel) * width * height)
    };
}

void data_free(struct pixel* data) {
    free(data);
}

void image_free(struct image *image) {
    data_free(image->data);
}

